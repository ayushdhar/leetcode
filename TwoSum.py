def twoSum(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: List[int]
    """
    for i in nums:
        if target - i in nums[nums.index(i) + 1:]:
            return [nums.index(i), nums[nums.index(i) + 1:].index(target - i) + 1 + nums.index(i)]
