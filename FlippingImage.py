class Solution:
    def flipAndInvertImage(self, A):

        A = [i[::-1] for i in A]

        for i in A:
            for j in range(len(i)):
                if i[j] == 0:
                    i[j] = 1
                elif i[j] == 1:
                    i[j] = 0
        return A

class Solution(object):
    def flipAndInvertImage(self, A):
        for row in A:
            for i in xrange((len(row) + 1) / 2):
                """
                In Python, the shortcut row[~i] = row[-i-1] = row[len(row) - 1 - i]
                helps us find the i-th value of the row, counting from the right.
                """
                row[i], row[~i] = row[~i] ^ 1, row[i] ^ 1
        return A

