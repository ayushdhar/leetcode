class Solution:
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        if len(str(num)) == 1:
            return num

        if num > 9 and num % 9 != 0:
            return num % 9
        else:
            return 9