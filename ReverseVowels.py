class Solution:
    def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """
        vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]

        s = list(s)
        found = list()
        pos = list()

        for i in s:

            if i in vowels:
                found.insert(0, i)
                pos.append(s.index(i))
                s.insert(s.index(i), 0)
                s.remove(i)

        for i in reversed(range(len(s))):

            if s[i] == 0:
                s[i] = found.pop()




        return "".join(s)